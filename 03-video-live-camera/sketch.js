

var myHandsfree;
//making vars  to print random sentences
let a = 'I can face you only when you look at me';
let b = 'this is the real ';
let c = 'I make the memories';
let d = 'I erase them';
let e = 'and you seem to be still';
let f = 'but stillness is the fractional situation of the movement';
let g = 'nothing is still';
let h = 'you are set up like crazy to draw and draw and draw and draw';
let i = 'till you cease to exist';
let j = 'by your own overlapping';
let k = 'you are now smudge and letters';
let sentences;
//let em;
//let n;
//let o;
//let p;


function setup() {
  createCanvas(1280, 720);
  //fill(0);
  // text(a, 300, 300, 300, 80); // Text wraps within text box
  // text(a,10, 90); // Text wraps within text box
  sentences = [a, b, c, d, e, f, g, h, i, j, k];
  let sentence = random(sentences);
  text(sentence, 60, 20);


  var myConfig = {
    hideCursor: true
  };
  myHandsfree = new Handsfree(myConfig);
  myHandsfree.start();

}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}

function mousePressed() {
  // χρησιμοποιείται για τη διαχείριση του mouse Click
  if (mouseX > 0 && mouseX < 100 && mouseY > 0 && mouseY < 100) {
    // ON/OFF fullscreen
    let fs = fullscreen();
    fullscreen(!fs);
  }
}


function draw() {
  //background(255);

  if (myHandsfree.isTracking) {
    if (myHandsfree.pose.length > 0) {

      var face0 = myHandsfree.pose[0].face;
      var nPoints = face0.vertices.length;

      //fill(255, 255, 0);
      for (var i = 0; i < nPoints; i += 2) {
        var x = face0.vertices[i + 0];
        var y = face0.vertices[i + 1];
        //ellipse(x*1.2, y*1.07, 1/20, 1/30,800);

        ellipse(x * 1.2, y * 1.07, 1 / 20, 1 / 20, 1105);


      }

      // Rotations of the head, in radians
      var rx = face0.rotationX; // pitch
      var ry = face0.rotationY; // yaw
      var rz = face0.rotationZ; // roll



    }
  }





}


function mouseClicked() {
  let darkness = 0;
  for (var i = mouseX-10; i < mouseX+9; i++) {
    for (var j = mouseY-10; j < mouseY+9; j++) {
      let colors = get(i, j);
      darkness += colors[3];
    }
  }
  console.log(darkness);
  if (darkness > 15000) {
    let sentence = random(sentences);
    text(sentence, mouseX, mouseY);
  }
}
function keyPressed() {
  if (keyCode === ENTER)
    redraw();
}