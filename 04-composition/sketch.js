

var myHandsfree;

//---------------------------------------------
function setup() {
  createCanvas(1280, 720);

  var myConfig = {
    hideCursor: true
  };
  myHandsfree = new Handsfree(myConfig);
  myHandsfree.start();

}

//---------------------------------------------
function draw() {
  background(255);

  if (myHandsfree.isTracking) {
    if (myHandsfree.pose.length > 0) {

      var face0 = myHandsfree.pose[0].face;
      var nPoints = face0.vertices.length;

      fill(255, 0, 0);


      // Rotations of the head, in radians
      var rx = face0.rotationX; // pitch
      var ry = face0.rotationY; // yaw
      var rz = face0.rotationZ; // roll

      if (detectBlinks(face0)) {
        background(0);
        console.log("blink!");
        fill(255,255,255);
        for (var i = 0; i < nPoints; i += 2) {
          var x = face0.vertices[i + 0];
          var y = face0.vertices[i + 1];
          ellipse(x * 1.2, y * 1.07, 1 / 20, 1 / 20, 1105);
          stroke(255);
          //ellipse(x, y, 9, 9);
          // text((i / 2).toString(), x, y);
        }


      }

    }

    function detectBlinks(face0) {

      var y37 = face0.vertices[2 * 37 + 1]
      var y41 = face0.vertices[2 * 41 + 1]

      var distance = Math.abs(y37 - y41);
      console.log(distance);

      if (distance < 5) return true;
      else return false;

    }
  }
}

function mousePressed() {
  // χρησιμοποιείται για τη διαχείριση του mouse Click
  if (mouseX > 0 && mouseX < 100 && mouseY > 0 && mouseY < 100) {
    // ON/OFF fullscreen
    let fs = fullscreen();
    fullscreen(!fs);
  }
}


function windowResized() {

  resizeCanvas(windowWidth, windowHeight);
}


function mouseClicked() {
  save('myCanvas.png')
  return false;
}

function keyPressed() {
  if (keyCode === ENTER)
    redraw();

}