

var img;
let offset=0
let easing=0.05



function preload(){
img=loadImage('myCanvas.png');
}


function setup() {
  createCanvas(1280, 720);
  image(img,0,0);
    filter(INVERT);
    }

    function draw() {
      image(img, 0, 0); // Display at full opacity
      let dx = mouseX - img.width / 2 - offset;
      offset += dx * 6* easing;
      tint(405, 45); // Display at half opacity
      filter(INVERT);
      image(img, offset, 0);
    }

   



function windowResized(){
  resizeCanvas(windowWidth, windowHeight);
}
 


function mousePressed() {
   if (mouseX > 0 && mouseX < 100 && mouseY > 0 && mouseY < 100) {
    // ON/OFF fullscreen
    let fs = fullscreen(); 
     fullscreen(!fs); 
  }
} ///λειτουργεί στον  web editor

function mouseClicked(){
  save('myCanvasNegativeMess.png') 
  return false;
 }


 function keyPressed() {
  if (keyCode===ESCAPE)
  redraw();  

}

